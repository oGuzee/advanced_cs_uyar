"use strict";

/**
 * Created by og on 28/01/18.
 */
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var routes = require('./routes/users');
var path = require('path');
var index = require('./routes/index');




/**
 * Create App and define routes
 * @type {*}
 */
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/', index);
app.use('/crud', routes);
app.use(express.static(path.join(__dirname, 'public')));


/**
 * View engine setup
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


/**
 * MongoDB
 */
mongoose.connect('mongodb://localhost/bachelor');
var db = mongoose.connection;
db.on('error', function () {
    console.log("Error: Connection to mongodb failed");
});
db.once('open', function () {
    console.log('Connection to mongodb successful');
});


/**
 * Start server on localhost:3000
 */
app.listen(3000, function (err) {
    if (err !== undefined) {
        console.log('Error on startup', err);
    }
    else {
        console.log('Server is running on port 3000');
    }
});