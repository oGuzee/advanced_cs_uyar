var express = require('express');
var userModel = require('../schema/userSchema');
var mongoose = require('mongoose');
var router = express.Router();

/**
 * Render the index.jade file
 */
router.get('/', function(req, res, next) {
    res.render('index', { layout : 'layout', json: res.locals.items });
});

module.exports = router;