var express = require('express');
var userRouter = express.Router();
var userModel = require('../schema/userSchema');
var utils = require('./utils');


/**
 * On base route / we have GET and POST
 */
userRouter.route('/')
    .get(function (req, res, next) {
        userModel.find()
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                if (users.length > 0) {
                    res.locals.items = users;
                    res.status(200);
                }
                next();
            })
    }, utils.sendResponse)
    .post(function (req, res, next) {
        var user = new userModel({
            name: req.body.name,
            description: req.body.description
        });
        user.save(function (err, user) {
            if (err) {
                return next(err);
            }
            res.locals.items = user;
            res.status(201);
            next();
        })
    }, utils.sendResponse);


/**
 *  On route /:id we have GET, PUT and DELETE
 */
userRouter.route('/:id')
    .get(function (req, res, next) {
        userModel.findOne({'_id': req.params.id})
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    var error = new Error('User doest found');
                    error.status = 404;
                    return next(error);
                }
                res.locals.items = user;
                res.status(200);
                next();
            })
    }, utils.sendResponse)
    .put(function (req, res, next) {
        userModel.findByIdAndUpdate({
                _id: req.params.id
            }, {
                $set: {
                    name: req.body.name,
                    description: req.body.description
                }
            }, {new: true}, function (err, item) {
                if (err) {
                    return next(err);
                }
                else {
                    res.locals.items = item;
                    res.status(200);
                    next();
                }
            }
        )
    }, utils.sendResponse)
    .delete(function (req, res, next) {
        userModel.findByIdAndRemove(req.params.id, {remove: true}, function (err, item) {
            if (err) {
                return next(err);
            }
            else if (!item) {
                return next(err);
            }
            res.status(200);
            next();
        });
    }, utils.sendResponse);

userRouter.use(utils.notFound);

module.exports = userRouter;